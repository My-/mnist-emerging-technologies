# Flask App
**Part-2** of the project.

### Project structure:
Bellow is project structure. Files are well commented so please check them for more info.
- `static`:
    - `js` - folder containing all JS code.
    - `models` - already contains trained model (99.2%)
    - `wasm` - is place where compiled wasm code should go. Please check [guide][guide-how-to-run] how to do it.
- `templates` - contains HTML file (web-app).
- `app.py` - Flask app.
- `flak-wasm.iml` - PyCharm IDE related file (can be ignored).
- `requirements.txt` - requirements file.

### About:
To understand what I did and why I would recommend check code and check issues.
- [Bad prediction][bad-prediction] issue documents how I prepared data.
- [Wasm][using-wasm] issue documents why I decided to use WebAssembley and what i did in it.
- [Create UI][app-ui] issue documents how UI (web-app) was build. Plus explains how to use it.

### References:
All Reference could be found inside the code and in related issues.



[guide-how-to-run]: https://gitlab.com/My-/mnist-emerging-technologies/blob/master/How-To-Run.md
[bad-prediction]: https://gitlab.com/My-/mnist-emerging-technologies/issues/9
[using-wasm]: https://gitlab.com/My-/mnist-emerging-technologies/issues/10
[app-ui]: https://gitlab.com/My-/mnist-emerging-technologies/issues/12
