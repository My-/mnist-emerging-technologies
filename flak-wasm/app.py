
import flask
from flask import Flask, render_template, send_file
from flask import request, jsonify
import base64
import PIL
from PIL import Image
from io import BytesIO
import keras as kr
import numpy as np
np.set_printoptions(threshold=np.inf)

app = Flask(__name__)


# Ref: https://stackoverflow.com/q/47858996/5322506
# ref: https://stackoverflow.com/a/42538142
# Ref: https://github.com/python-pillow/Pillow/issues/3400#issuecomment-428104239
def get_luma_from_base64(encoded_data):
    # Decode base64 url to `Luma` image
    decoded_data = base64.b64decode(encoded_data)
    img_data = BytesIO(decoded_data)
    img = Image.open(img_data).convert('L')
    return img


@app.route('/', methods=['GET', 'POST'])
def hello():
    # Main `Root` path. This path serves Html document (app).
    if request.method == 'GET':
        return render_template('base.html'), 200


@app.route('/predict', methods=['GET', 'POST'])
def predict():
    # This path takes data as Json in format:
    #   `{"image": "user drawn image as base 64 url"}`
    # Then does last steps to mach training data and
    # then loads model and try to predict drawn number.
    if request.method == 'POST':
        if request.is_json:
            data = request.get_json()
            # print(data['image'])
            img = get_luma_from_base64(data['image'])
            # last steps image manipulation before prediction.
            data = ~np.array(img, dtype='int64')
            data = kr.utils.normalize(data, axis=0)
            data = data.reshape((1, 28, 28, 1))
            # Load model and predict. Then send result back tu user.
            # Note: Loading model here is not efficient. Better would be load outside
            # this function and then use it here then needed. But for simplicity
            # I left it here.
            model = kr.models.load_model('./static/models/cnn3-bn-9920-7e.model')
            model.summary()
            predictions = model.predict(data)
            print(predictions)
            # Send back prediction array
            return jsonify(predictions[0].tolist()), 201
        else:
            # If `GET` request send error back.
            return 'Wrong request format', 400


@app.route('/wasm/pkg/img_prep_wasm_bg.wasm')
def wasm_file():
    # Route to serve wasm file. It is fix to the problem I had with wasm.
    # Ref: https://stackoverflow.com/a/58962367/5322506
    return send_file('./static/wasm/pkg/img_prep_wasm_bg.wasm', mimetype='application/wasm')


@app.route('/js/prediction-chart.js')
def prediction_chart_file():
    # Route to serve wrapper I wrote for the `chart.js` chart I use.
    return send_file('./static/js/prediction-chart.js', mimetype='application/js')


@app.route('/versions')
def status():
    # Route to show versions of the software I use.
    import tensorflow as tf     # import just to get version
    # return versions used in project
    res = f'Keras version: {kr.__version__},'
    res += f'NumPy version: {np.version.version},'
    res += f'TensorFlow: {tf.__version__},'
    res += f'PIL version: {PIL.__version__},'
    res += f'flask version: {flask.__version__}'
    return jsonify('{' + res + '}'), 200


if __name__ == '__main__':
    app.run(host='0.0.0.0')
