/**
 * Prediction Chart draws predictions as bar graph.
 * It is wrapper over `chart.js` library to use only what I need.
 * More on library:
 *      Ref: https://www.chartjs.org/
 */
export default class PredictionChart {
    constructor(canvasId){
        const predictions = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

        this.ctx = document.getElementById(canvasId).getContext('2d')
        // ref: https://www.chartjs.org/docs/latest/getting-started/usage.html?h=data
        this.predictionChart = new Chart(this.ctx, {
            type: 'bar',
            data: {
                labels: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
                datasets: [{
                    label: 'Prediction',
                    data: predictions,
                    backgroundColor: [
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(153, 102, 247, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        })
    }

    // Ref: https://www.chartjs.org/docs/latest/developers/updates.html
    updateData({data}){
        this.predictionChart.data.datasets.forEach((dataset) => {
            console.log(`updating data: ${data}`)
            dataset.data = data
        });
        this.predictionChart.update()
    }
}
