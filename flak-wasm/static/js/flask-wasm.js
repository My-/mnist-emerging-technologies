/**
 * The main logic code. It is loads Wasm module and calls it's function (async).
 * Then takes data from small preview canvas (canvas-28x28) and sends it to server (Flask).
 * Then takes response (async) and displays predictions in chart graph to the user.
 */
import wasmInit from "../wasm/pkg/img_prep_wasm.js"
import PredictionChart from "./prediction-chart.js"


const chartCanvasId = 'chart-canvas'
const predictionChart = new PredictionChart(chartCanvasId)

const runWasm = async () => {
    // load dummy data "all 100%" to chart
    const dummyData = [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]
    predictionChart.updateData({data: dummyData})

    // import wasm file
    const rustWasm = await wasmInit("../wasm/pkg/img_prep_wasm_bg.wasm")
    // run Rust (wasm) function
    rustWasm.prepareImg()
    // get canvas holding populated data from Wasm
    const canvas_view = document.getElementById('canvas-28x28')

    // get it's data
    const dataUrl = canvas_view.toDataURL()
    const data = dataUrl.split(',')[1]

    // Send that data to the Flask sever to predict
    const predictions = await postData('/predict', {'image' : data})

    console.log(`Predictions: ${predictions}`)

    // Update chart
    predictionChart.updateData({data: predictions})

    // Get index of biggest value
    // Ref: https://stackoverflow.com/a/51523641/5322506
    const indexOfMaxValue = predictions.indexOf(Math.max(...predictions));
    console.log(`Network thinks is number: ${indexOfMaxValue}`)
}

// Ref: https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
async function postData(url = '', data = {}) {
    // Default options are marked with *
    const response = await fetch(url, {
        method: 'POST', // *GET, POST, PUT, DELETE, etc.
        mode: 'cors', // no-cors, *cors, same-origin
        cache: 'no-cache', // *default, no-cache, reload, force-cache, only-if-cached
        credentials: 'same-origin', // include, *same-origin, omit
        headers: {
            'Content-Type': 'application/json'
            // 'Content-Type': 'application/x-www-form-urlencoded',
        },
        redirect: 'follow', // manual, *follow, error
        referrer: 'no-referrer', // no-referrer, *client
        body: JSON.stringify(data) // body data type must match "Content-Type" header
    });
    return await response.json(); // parses JSON response into native JavaScript objects
}

// Ref: https://stackoverflow.com/questions/49338193/how-to-use-code-from-script-with-type-module
document.querySelector('#button-send').addEventListener('click', runWasm)
