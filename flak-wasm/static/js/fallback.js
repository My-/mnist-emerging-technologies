/**
 * Fallback code if wasm module is not loaded.
 */

function runWasm() {
    console.log(`Fallback mode. Wasm not loaded`)
}