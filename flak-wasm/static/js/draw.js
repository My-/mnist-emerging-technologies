/**
 * This code responsible for drawing on canvas.
 *
 *  Code adapted from:
 *      https://developer.mozilla.org/en-US/docs/Web/API/Element/mousedown_event
 *      https://refork.com/xfcc
 */


const canvas = document.getElementById('drawing-canvas')
const ctx = canvas.getContext('2d')

clearArea()

// When true, moving the mouse draws on the canvas
let isDrawing = false
let x = 0
let y = 0


// The x and y offset of the canvas from the edge of the page
const rect = canvas.getBoundingClientRect()

// Add the event listeners for mousedown, mousemove, and mouseup
canvas.addEventListener('mousedown', e => {
    x = e.clientX - rect.left
    y = e.clientY - rect.top
    isDrawing = true
})

canvas.addEventListener('mousemove', e => {
    if (isDrawing) {
        drawLine(ctx, x, y, e.clientX - rect.left, e.clientY - rect.top)
        x = e.clientX - rect.left
        y = e.clientY - rect.top
    }
})

window.addEventListener('mouseup', e => {
    if (isDrawing) {
        drawLine(ctx, x, y, e.clientX - rect.left, e.clientY - rect.top)
        x = 0
        y = 0
        isDrawing = false
    }
})

function drawLine(context, x1, y1, x2, y2) {
    // adjust drawing pointer position
    x1 -= 25
    y1 -= 20
    x2 -= 25
    y2 -= 20
    context.beginPath()
    context.strokeStyle = '#F5F5F5' // dirty white
    context.lineWidth = 6
    context.lineJoin = 'round'
    context.lineCap = 'round'
    context.moveTo(x1, y1)
    context.lineTo(x2, y2)
    context.closePath()
    context.stroke()
}

// https://refork.com/xfcc
function clearArea() {
    ctx.fillStyle = "black";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
}