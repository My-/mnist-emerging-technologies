#!/usr/bin/env bash

# copy generated pkg directory to flask app wasm/pkg directory
# https://stackoverflow.com/a/24486142/5322506
cp -TRv ./pkg/ ~/GMIT/EmTech/mnist-emerging-technologies/flak-wasm/static/wasm/pkg