#[derive(Debug)]
pub struct ImgData <'a>{
    width: u32,
    height: u32,
    data: &'a [u8],
}

impl <'a>ImgData<'a> {
    pub fn from(data: &'a [u8], width: u32, height: u32) -> Self {
        Self {width, height, data }
    }
}


#[derive(Debug)]
pub struct ImgLimits {
    min_x: u32,
    max_x: u32,
    min_y: u32,
    max_y: u32,
}

impl ImgLimits {
    ///
    /// Creates initial ImgLimits were max values is '0' and min x = width, min y = height.
    ///
    pub fn from(width: u32, height: u32) -> Self {
        Self {
            min_x: width,
            max_x: 0,
            min_y: height,
            max_y: 0,
        }
    }

    pub fn from_data(img: &ImgData, background: u8) -> Self {
        let mut limits = ImgLimits::from(img.width, img.height);
        for (i, pixel) in img.data.iter().enumerate() {
            if pixel > &background {
                let pos = (
                    i as u32 % img.width,
                    i as u32 / img.height
                );
                limits.update(pos)
            }
        }

        limits
    }

    ///
    /// Updates limits if needed
    ///
    pub fn update(&mut self, pos: (u32, u32)) {
        let (x, y) = pos;

        if self.min_x > x {
            self.min_x = x;
        }

        if self.max_x < x {
            self.max_x = x;
        }

        if self.min_y > y {
            self.min_y = y;
        }

        if self.max_y < y {
            self.max_y = y;
        }
    }

    ///
    /// Returns offset (x, y) needed to add to make square out of limits
    ///
    pub fn get_difference(&self) -> (u32, u32) {
        let (size_x, size_y) = (self.max_x - self.min_x, self.max_y - self.min_y);

        if size_x < size_y {
            return (size_y - size_x, 0);
        }
        else if size_x > size_y {
            return (0, size_x - size_y);
        }
        else {
            return (0, 0);
        }
    }

    ///
    /// Gets limits width and height
    ///
    pub fn get_width_height(&self) -> (u32, u32) {
        let width = self.max_x -self.min_x;
        let height = self.max_y - self.min_y;

        (width, height)
    }

    ///
    /// Gets distances to make limits as square area.
    /// (left, right, top, bttm)
    ///
    pub fn get_make_square_distances(&self) -> (u32, u32, u32, u32) {
        let (mut top, mut right, mut bttm, mut left) = (0, 0, 0, 0);
        let (offset_x, offset_y) = self.get_difference();

        if offset_x < offset_y {
            let offset = offset_y - offset_x;
            top = offset / 2;
            bttm = offset - offset / 2;
        }
        else if offset_x > offset_y {
            let offset = offset_x - offset_y;
            left = offset / 2;
            right = offset - offset / 2;
        }

        (left, right, top, bttm)
    }

    pub fn get_x(&self) -> (u32, u32) {
        (self.min_x, self.max_x)
    }

    pub fn get_y(&self) -> (u32, u32) {
        (self.min_y, self.max_y)
    }

    pub fn get_min(&self) -> (u32, u32) {
        (self.min_x, self.min_y)
    }

    pub fn get_max(&self) -> (u32, u32) {
        (self.max_x, self.max_y)
    }
}