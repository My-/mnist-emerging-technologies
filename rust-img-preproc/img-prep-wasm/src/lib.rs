// Run it: wasm-pack build --target web
mod utils;
// Ref: https://wasmbyexample.dev/examples/webassembly-linear-memory/webassembly-linear-memory.rust.en-us.html
mod img_limits;
mod drawing_canvas;

extern crate js_sys;
extern crate wasm_bindgen;
extern crate image;

// The wasm-pack uses wasm-bindgen to build and generate JavaScript binding file.
// Import the wasm-bindgen crate.
use wasm_bindgen::prelude::*;
use std::u8;
use image::{ImageBuffer, Luma, imageops, FilterType, DynamicImage};

use img_limits::ImgLimits;
use crate::drawing_canvas::{DrawingCanvas, LineParam};
use crate::img_limits::ImgData;

// First up let's take a look of binding `console.log` manually, without the
// help of `web_sys`. Here we're writing the `#[wasm_bindgen]` annotations
// manually ourselves, and the correctness of our program relies on the
// correctness of these annotations!

// ref: https://rustwasm.github.io/docs/wasm-bindgen/examples/console-log.html
#[wasm_bindgen]
extern "C" {
    // Use `js_namespace` here to bind `console.log(..)` instead of just
    // `log(..)`
    #[wasm_bindgen(js_namespace = console)]
    fn log(s: &str);

    // The `console.log` is quite polymorphic, so we can bind it with multiple
    // signatures. Note that we need to use `js_name` to ensure we always call
    // `log` in JS.
    #[wasm_bindgen(js_namespace = console, js_name = log)]
    fn log_u32(a: u32);

    // f64
    #[wasm_bindgen(js_namespace = console, js_name = log)]
    fn log_f64(a: f64);

    // Multiple arguments too!
    #[wasm_bindgen(js_namespace = console, js_name = log)]
    fn log_many(a: &str, b: &str);
}

// Next let's define a macro that's like `println!`, only it works for
// `console.log`. Note that `println!` doesn't actually work on the wasm target
// because the standard library currently just eats all output. To get
// `println!`-like behavior in your app you'll likely want a macro like this.

macro_rules! console_log {
    // Note that this is using the `log` function imported above during
    // `bare_bones`
    ($($t:tt)*) => (log(&format_args!($($t)*).to_string()))
}



//// Adopted from: https://rustwasm.github.io/docs/wasm-bindgen/examples/2d-canvas.html
#[wasm_bindgen]
#[allow(non_snake_case)]
pub fn prepareImg() -> Result<(), JsValue> {
    // Create DrawingCanvas
    let drawing_canvas_id = "drawing-canvas";
    let drawing_canvas = DrawingCanvas::from_window_with_id(drawing_canvas_id)?;
    // get canvas data
    let data = drawing_canvas.get_data()?;

    //    console::log_1(&data);
//    console_log!("{:?}", &data);
    console_log!("Image size: height: {}, width: {}", drawing_canvas.width(), drawing_canvas.height());

    let img = get_first_chanel(&data);

    console_log!("img arr size: {:?}", img.len());
//    console_log!("img: {:?}", img);
//    log_image(canvas_width, canvas_height, &img);


    // set initial limits
    let limits = ImgLimits::from(drawing_canvas.width(), drawing_canvas.height());
    console_log!("Initial limits: {:?};", limits);

    // g
    // et image boundaries
    let img_data = ImgData::from(&img, drawing_canvas.width(), drawing_canvas.height());
    let limits = ImgLimits::from_data(&img_data, 0_u8);
    console_log!("{:?}", limits);


    // Draw boundaries on canvas
    let line_red_1 = LineParam::from("red", 1.0);
    drawing_canvas.rectangle(limits.get_min(), limits.get_max(), &line_red_1)?;


    // width height and size of the limit
    let (width,height) = limits.get_width_height();
    console_log!("height: {}, width: {}", height, width);
    let size = height * width;
    let size_as_square = match height > width {
        true => height * height,
        false => width * width,
    };
    console_log!("img({}px, {}px) = {}px ({}px)", width, height, size, size_as_square);
    // get distances to make it square
    let (left, right, top, bttm) = limits.get_make_square_distances();
    console_log!("top: {}, bttm: {}, left: {}, right: {}", top, bttm, left, right);


    // calculate offsets
    let (offset_x, offset_y) = limits.get_difference();
    console_log!("offset_x: {}, offset_y: {}", offset_x, offset_y);
    if offset_x + offset_y == 0 {
        console_log!("Empty image!");
        return Err(JsValue::from_str("Canvas has no data. Empty image!!"));
    }


    // crop image
    let mut image = ImageBuffer::<Luma<u8>, _>::from_raw(
        drawing_canvas.width(),
        drawing_canvas.height(),
        img
    ).expect("cant load image");
//    console_log!("image: {:?}", image);
    let subimg = imageops::crop(
        &mut image,
        limits.get_x().0 +1,
        limits.get_y().0 +1,
        width, height
    );
//    console_log!("subimg: {:?}", subimg);


    // Calculate cente of mass
    let sub_image:Vec<u8> = subimg.to_image().into_vec();
    let (mass_x, mass_y) = get_centre_of_mass(width, &sub_image);
    console_log!("Sub image: width: {}, height: {}", width, height);
    console_log!("Sub image mass: x: {}, y: {}", mass_x, mass_y);


    // Draw lines on the centre of mass
    // X
    let param_blue_1 = LineParam::from("blue", 1.0);
    let mass_line_start = (mass_x as u32 + limits.get_x().0, limits.get_y().0);
    let mass_line_end   = (mass_x as u32 + limits.get_x().0, limits.get_y().1);
    drawing_canvas.line(mass_line_start, mass_line_end, &param_blue_1)?;
    // Y
    let mass_line_start = (limits.get_x().0, mass_y as u32 + limits.get_y().0);
    let mass_line_end   = (limits.get_x().1, mass_y as u32 + limits.get_y().0);
    drawing_canvas.line(mass_line_start, mass_line_end, &param_blue_1)?;

    // Resize image
    // load
    let image =
        ImageBuffer::<Luma<u8>, _>::from_raw(width, height, sub_image)
            .expect("cant load image");
    // resized image width and height
    let (res_width, res_height) = match width < height {
        true => ((width as f64 * 20.0 / height as f64) as u32, 20),
        false => (20, (height as f64 * 20.0 / width as f64) as u32),
    };
    // resize image
    let resized_img = imageops::resize(
        &image,
        res_width,
        res_height,
        FilterType::Triangle
    );

//    log_image(res_width, res_height, &resized_img);


    // Create mnist element
    // mass
    let (mass_x, mass_y) = get_centre_of_mass(res_width, &resized_img);
    console_log!("Mass: x: {}, y: {}", mass_x, mass_y);
    // mnist size 28x28
    let (mnist_width, mnist_height) = (28, 28);
    // create empty background
    let mut mnist_element = DynamicImage::new_luma8(mnist_width, mnist_height);
    // calculate overlays
    let overlay_x = mnist_width as f64 / 2.0 - mass_x;
    let overlay_y = mnist_height as f64 / 2.0 - mass_y;
    // overlay images
    let resized_img = DynamicImage::ImageLuma8(resized_img);
    // https://docs.rs/image/0.22.3/image/imageops/fn.overlay.html
    image::imageops::overlay(
        &mut mnist_element,
        &resized_img,
        overlay_x.round() as u32,
        overlay_y.round() as u32
    );


    // Put image to canvas-28x28
    let display_canvas_id = "canvas-28x28";
    let mut display_canvas = DrawingCanvas::from_window_with_id(display_canvas_id)?;
    // make it bgra
    let mnist_element_bgra = mnist_element.to_bgra();
    let data = &mut mnist_element_bgra.to_vec()[0..];
    display_canvas.set_data(data)?;

    Ok(())
}

// get first chanel from image data (make b/w)
fn get_first_chanel(img: &[u8]) -> Vec<u8> {
    // image B/W
    let mut arr: Vec<u8> = Vec::new();

    // take single chanel from the image
    for (i, d) in img.iter().enumerate() {
        // get one channel. For B/W image doesnt matter witch.
        if i % 4 == 0 {
            arr.push(*d);
        }
    }

    arr
}

// Calculate images centre of mass
fn get_centre_of_mass(width: u32, img: &[u8]) -> (f64, f64) {
    // Centre of mass
    let mut pixel_sum = 0_usize;
    let (mut sum_x, mut sum_y) = (0_f64, 0_f64);

    for (i, pixel) in img.iter().enumerate(){
        if *pixel != 0 {
            pixel_sum += *pixel as usize;
            let (pos_x, pos_y) = get_position_from_index(i, width);
            sum_x += *pixel as f64 * pos_x as f64;
            sum_y += *pixel as f64 * pos_y as f64;
//            console_log!("Pos({}, {}), Sum({}, {})", pos_x, pos_y, sum_x, sum_y);
        }
    }

    let (mass_x, mass_y) = (sum_x / pixel_sum as f64, sum_y / pixel_sum as f64);

    (mass_x, mass_y)
}

// get x and y from index
fn get_position_from_index(index: usize, image_width: u32) -> (u32, u32) {
    let x = index as u32 % image_width;
    let y = index as u32 / image_width;

    (x, y)
}

// get index from given position
fn get_index_from_position(pos: (u32, u32), image_width: u32) -> usize {
    let (x, y) = pos;

    (y * image_width + x) as usize
}

// log image to console for debuging purpoces
#[allow(dead_code)]
fn log_image(width: u32, height: u32, img: &[u8]){
    if (width * height) as usize != img.len() {
        console_log!("\nLog Image got: width: {}, height: {}, but image size is: {}", width, height, img.len());
    }
    else{
        console_log!("\nImage: height: {}, width: {}, pixels: {}", height, width, height * width);
        let mut row = String::with_capacity(width as usize);
        for y in 0..height {
            row.clear();
            for x in 0..width {
                let pixel =img[get_index_from_position((x, y), width)];
                let pixel = match pixel {
                    0 => ' ',
                    1..=63 => '.',
                    64..=127 => '*',
                    128..=192 => 'x',
                    _ => 'X',
                };
                row.push(pixel)
            }
            console_log!("{}", row);
        }
        console_log!("");
    }
}
