extern crate wasm_bindgen;

use wasm_bindgen::{JsValue, JsCast, Clamped};


#[derive(Debug)]
pub struct LineParam <'a> {
    color: &'a str,
    width: f64,
}

impl <'a> LineParam <'a> {
    pub fn from(color: &'a str, width: f64) -> Self {
        Self { color, width }
    }
}

#[derive(Debug)]
pub struct DrawingCanvas {
    context: web_sys::CanvasRenderingContext2d,
    width : u32,
    height: u32,
}

impl DrawingCanvas {
    pub fn from_window_with_id(id: &str) -> Result<Self, JsValue> {
        // https://rustwasm.github.io/wasm-bindgen/api/web_sys/fn.window.html
        let window = web_sys::window()
            .expect("No global `window` exists");
        // https://rustwasm.github.io/wasm-bindgen/api/web_sys/struct.Window.html#method.document
        let document = window.document()
            .expect("Should have a document on window");
        // https://rustwasm.github.io/wasm-bindgen/api/web_sys/struct.Document.html#method.get_element_by_id
        let canvas = document.get_element_by_id(id)
            .expect(format!("Can't find element with id: {}", id).as_str());

        let canvas: web_sys::HtmlCanvasElement = canvas
            // https://rustwasm.github.io/wasm-bindgen/api/wasm_bindgen/trait.JsCast.html#method.dyn_into
            .dyn_into::<web_sys::HtmlCanvasElement>()
            .map_err(|_| ())
            .unwrap();

        let width = canvas.width();
        let height = canvas.height();

        let context = canvas
            .get_context("2d")
            .unwrap()
            .unwrap()
            .dyn_into::<web_sys::CanvasRenderingContext2d>()
            .unwrap();

        Ok(Self { context, width, height })
    }

    pub fn line(&self, start: (u32, u32), end: (u32, u32), line_param: &LineParam ) -> Result<(), JsValue> {
        let color = line_param.color;
        let width = line_param.width;
        let (start_x, start_y) = start;
        let (end_x, end_y) = end;

        self.context.begin_path();
        let stroke_color = JsValue::from_str(color);
        // https://rustwasm.github.io/wasm-bindgen/api/web_sys/struct.CanvasRenderingContext2d.html#method.set_stroke_style
        self.context.set_stroke_style(&stroke_color);
        // https://rustwasm.github.io/wasm-bindgen/api/web_sys/struct.CanvasRenderingContext2d.html#method.set_line_width
        self.context.set_line_width(width);
        // https://rustwasm.github.io/wasm-bindgen/api/web_sys/struct.CanvasRenderingContext2d.html#method.move_to
        self.context.move_to(start_x as f64, start_y as f64);
        // https://rustwasm.github.io/wasm-bindgen/api/web_sys/struct.CanvasRenderingContext2d.html#method.line_to
        self.context.line_to(end_x as f64, end_y as f64);
        // https://rustwasm.github.io/wasm-bindgen/api/web_sys/struct.CanvasRenderingContext2d.html#method.stroke
        self.context.stroke();

        Ok(())
    }

    pub fn rectangle(&self, top_left: (u32, u32), bttm_right: (u32, u32), line_param: &LineParam) -> Result<(), JsValue> {
        let tl = top_left;
        let tr = (bttm_right.0, top_left.1);
        let bl = (top_left.0, bttm_right.1);
        let br = bttm_right;

        self.line(tl, tr, line_param)?;
        self.line(tr, br, line_param)?;
        self.line(br, bl, line_param)?;
        self.line(bl, tl, line_param)?;


        Ok(())
    }

    pub fn get_data(&self) -> Result<Vec<u8>, JsValue> {
        // https://rustwasm.github.io/wasm-bindgen/api/web_sys/struct.CanvasRenderingContext2d.html#method.get_image_data
        let img_data = self.context.get_image_data(0 as f64, 0 as f64, self.width as f64, self.height as f64)?;
        // https://rustwasm.github.io/wasm-bindgen/api/web_sys/struct.ImageData.html#method.data
        let data = img_data.data();

        Ok(data.to_vec())
    }

    pub fn set_data(&mut self, data: &mut [u8]) -> Result<(), JsValue> {
        let clamped = Clamped(data);
        let image_data = web_sys::ImageData::new_with_u8_clamped_array(clamped, self.width)?;

        self.context.put_image_data(&image_data, 0_f64, 0_f64)?;

        Ok(())
    }

    pub fn width(&self) -> u32 {
        self.width
    }

    pub fn height(&self) -> u32 {
        self.height
    }
}