# How to Run Guide

**Note:** as I don't have Windows machine I not guarantee it will work on it. But I believe it should with some adjustments.

### tl;dr
I created docker image to test app. I believe it is the easiest way to test project.
Decker image cant be found [here](https://hub.docker.com/repository/docker/fbcs/mnist-web-app)
```bash
# open terminal and run
docker run --name=mindaugas-app -d -p 5000:5000 fbcs/mnist-web-app
```
Open browser and go to `localhost:5000`

### Prerequisites
You will need to have:
- [Anaconda](https://www.anaconda.com/distribution/).

```bash
# Anaconda version
mindaugas@myMint:~$ conda -V
conda 4.7.12

# Python
mindaugas@myMint:~$ python -V
Python 3.7.4

# using build in endpoint
mindaugas@myMint:~$ wget -qO- 127.0.0.1:5000/versions
"{Keras version: 2.3.1,NumPy version: 1.17.3,TensorFlow: 1.14.0,PIL version: 6.2.1,matplotlib version: 3.1.1,flask version: 1.1.1}"

# Flask version
mindaugas@myMint:~/$ flask --version
Python 3.7.4
Flask 1.1.1
Werkzeug 0.16.0

```
- [Rust](https://doc.rust-lang.org/1.0.0/book/installing-rust.html) for the Wasm part

```bash
# rust compiler
mindaugas@myMint:~$ rustc -V
rustc 1.39.0 (4560ea788 2019-11-04)
# cargo
mindaugas@myMint:~$ cargo -V
cargo 1.39.0 (1c6ec66d5 2019-09-30)
# and rust up
mindaugas@myMint:~$ rustup -V
rustup 1.20.2 (13979c968 2019-10-16)

```

- Modern browser (eg. Firefox)

### Running project:

```bash
# Clone project
git clone https://gitlab.com/My-/mnist-emerging-technologies.git

# go into project directory
cd mnist-emerging-technologies/

# you need to install wasm-pack by:
cargo install wasm-pack

# Compile Rust code to wasm
wasm-pack build --target web ./rust-img-preproc/img-prep-wasm

# copy generated Wasm binaries to flask app
cp -TRv ./rust-img-preproc/img-prep-wasm/pkg/ ./flak-wasm/static/wasm/pkg

# install requirements
pip install -r ./flak-wasm/requirements.txt

# Go to flask app folder
cd flak-wasm

# export path
export FLASK_APP=./app

# run Flask
run flask

```

Open browser and go to `127.0.01:5000`
Test it :)
