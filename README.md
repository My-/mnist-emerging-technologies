# MNIST Emerging Technologies

### About the project
**Task:** Create app were user could draw digit and app should try to predict it.

**Project goals:**
- Create neural network and train it using [MNIST][lecun-site] data set.
- Use [Keras][keras] deep learning library and [Jupyter Notebook][jupyter-notebook] for this.
- Crate simple web-app for testing trained model.
- Use [Flask][flask] asynchronous JS calls and any other appropriate tool for this.
- Have a clean code.


**Not goals:**
- *To crate super high precision model* (~99% is perfect). Journey is more importand compare to perfect result.
- *Create best looking cutting edge web-app using latest frameworks.* Web-app is here for demonstrating purposes so it should be simple and functional.
- *Have industry standards meeting code.*

### How app was done:
- [Jupyter Notebook][jupyter-notebook] is used to prepare training data and train the model (network). [More here][project-jupyter].
- [Flask][flask] used to serve HTML page (web-app) were user can test trained network. [More here.][project-app]

### Repo Structure:
- `data` - contains MNIST data set from [here][lecun-site]. It is here just for convenience.
- `flak-wasm` - Flask app (I know it has typo). This is version 2. In it I used Wsam to prepare image for the training on client bowser. Version 1 [was deleted](https://gitlab.com/My-/mnist-emerging-technologies/commit/4ccf1aeb73e7f8478d3c005b0fb61a89012f456a) to clean the repo.
- `repo-resurces` - contains images used in some issues and my thoughts while I trying figure out what should I code.
- `rust-img-preproc` - contains code for preparing image. Code is written in Rust. All this code is compiled to Wasm and is run inside the browser.
- `testing` - contains Jupyter Notebook learning code. Note it's not my trained model, it is examples from the web I used to test how things work, check training performance and so on.
- `How-To-Run.md` - guide how to setup and run project.
- `README.md` - this file.
- `my-mnist.ipynb` - Jupyter code showing my attempt to train model.


### How to run Project:
- Please [check this guide](https://gitlab.com/My-/mnist-emerging-technologies/blob/master/How-To-Run.md).

### Technologies used:
- Docker - adopted [existing][ermaker-jupyter-docker] Jupyter docker image to [my needs][my-jupyter-docker] to help me train the model.
- [Jupyter Notebook][jupyter-notebook] to train model.
- Python version: 3.7
- Keras version: 2.2.4
- Tensorflow version: 1.14.0
- [Flask][flask] as web server.
- JavaScript features:
    - [ES6 classes][es6-classes] to make code more cleaner,
    - [Fetch API][fetch-api] to send asynchronously data to server.
    - [Async/await][async-await] syntax to get asynchronous calls data.
- [WebAssembley (Wasm)][wasm] to:
    - find image boundaries,
    - crop image,
    - resize image,
    - overlay two images,
    - draw debugging info on canvas.

### More on project:
Here are some notable project issues/parts:
- [Model predicts very bad][bad-prediction].
- [Using WebAssembly][using-wasm]
- [App UI and how to use it][app-ui]
- GitLab [Boards](https://gitlab.com/My-/mnist-emerging-technologies/-/boards) to check more project issues.
- [Project Graph](https://gitlab.com/My-/mnist-emerging-technologies/-/network/master) to check commits and branches history.
- [Project stats](https://gitlab.com/My-/mnist-emerging-technologies/-/graphs/master/charts). Interestingly Rust are second most used language after Jupyter in this project. Is hard resist to Rust ["Love"][rust-love] then you get used to it :)


### Final Word:
I read once what training model takes 20% of all time and all the rest 80% are spend on cleaning and preparing data. That was so true with this project. I had many "I had enough" moments but with help of some Rust "[Love][rust-love]" I managed make it work, finally. I'm quit happy how it ended up. Well I really wanted to try `tensorflow.js` but lack of time and requirement to use Flask made me give up this idea. Maybe next time ;)

### References


- [MINST in 10 Minutes ,Medium](https://towardsdatascience.com/image-classification-in-10-minutes-with-mnist-dataset-54c35b77a38d)
- [Tensorflow and deep learning - without a PhD by Martin Görner][tensorflow-without-phd]
- [MNIST data set][lecun-site]
- [Keras official site][keras]
- [Fetch API][fetch-api]
- Check files for more.



[lecun-site]: http://yann.lecun.com/exdb/mnist/
[keras]: https://keras.io/
[jupyter-notebook]: https://jupyter.org/
[flask]: https://flask.palletsprojects.com/en/1.1.x/
[tensorflow-without-phd]: https://www.youtube.com/watch?v=vq2nnJ4g6N0
[ermaker-jupyter-docker]: https://hub.docker.com/r/ermaker/keras-jupyter
[my-jupyter-docker]: https://hub.docker.com/r/fbcs/jupyter
[fetch-api]: https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API
[es6-classes]: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Classes
[async-await]: https://javascript.info/async-await
[wasm]: https://developer.mozilla.org/en-US/docs/WebAssembly
[rust-love]: https://www.reddit.com/r/rust/comments/bb8ij1/rust_is_the_most_loved_language_four_years_in_a/


[project-jupyter]: https://gitlab.com/My-/mnist-emerging-technologies/blob/master/my-mnist.ipynb
[project-app]: https://gitlab.com/My-/mnist-emerging-technologies/tree/master/flak-wasm


[bad-prediction]: https://gitlab.com/My-/mnist-emerging-technologies/issues/9
[using-wasm]: https://gitlab.com/My-/mnist-emerging-technologies/issues/10
[app-ui]: https://gitlab.com/My-/mnist-emerging-technologies/issues/12
